<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imovel extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Imovel_model');
	}

	public function index()
	{
		$imoveis = $this->Imovel_model->GetAll('ID_IMOVEL');
		$this->data['imoveis'] = $imoveis;
		$this->twig->display('imoveis', $this->data);
	}
}
