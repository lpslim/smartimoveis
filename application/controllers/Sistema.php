<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Imovel_model');
	}

	public function index()
	{
		$data['page'] = "Dashboard";
		$this->twig->display('dashboard/index', $data);
	}

	public function construtor()
	{
		$data['page'] = "Construtor";
		$this->twig->display('dashboard/construtor', $data);
	}

	public function mapa()
	{
		$data['page'] = "Mapa";
		$this->twig->display('dashboard/mapa', $data);
	}

	public function imoveis()
	{ 
		$queryImoveis = $this->Imovel_model->GetAll('ID_IMOVEL', 'desc');
		foreach ($queryImoveis as $imovel) {
			$imovel['endereco'] = $this->Imovel_model->GetEndereco($imovel["ID_IMOVEL"]);
			$imoveis[] = $imovel;
		}
		$data['imoveis'] = $imoveis;
		$data['page'] = "Imoveis";
		$this->twig->display('dashboard/imoveis', $data);
	}

	public function imovel($id)
	{
		$imovel = $this->Imovel_model->GetByField("ID_IMOVEL", $id);
		$imovel['endereco'] = $this->Imovel_model->GetEndereco($imovel["ID_IMOVEL"]);
		$num = 1;
		while (file_exists("Imagens/".$imovel["ID_IMOVEL"]."/".$num.".jpg")) {
			$imovel["fotos"][$num] = "/Imagens/".$imovel["ID_IMOVEL"]."/".$num.".jpg";
			$num++; 
		}
		$data['imovel'] = $imovel;
		$this->twig->display('dashboard/imovel', $data);
	}
}
