<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Imovel_model extends MY_Model {
	function __construct(){
		parent::__construct();
		$this->table = 'IMOVEIS';
	}

	function GetEndereco($ID_IMOVEL){
		$this->db->select("*");
		$this->db->from("ENDERECOS");
		$this->db->join($this->table, "ENDERECOS.ID_ENDERECO = IMOVEIS.ID_IMOVEL");
		$this->db->join("CIDADES", "ENDERECOS.ID_CIDADE = CIDADES.ID_CIDADE");
		$this->db->where("IMOVEIS.ID_IMOVEL", $ID_IMOVEL);
		$query = $this->db->get()->result();
		return $query[0];
	}
}