<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $data;

	public function __construct(){
		parent::__construct();
		$host = $_SERVER['HTTP_HOST'];
		if ($host == 'smartimoveis.dev') {
			redirect('/sistema');
		}else{
			$DB2 = $this->load->database('default', TRUE); 
			$DB2->from('clients')->where('domain', $host);
			$query = $DB2->get();
			if($query->num_rows() < 1)
	        	redirect ('errors');
			$host = $query->result();
			$this->data['host'] = $host[0];
		}

	}

}